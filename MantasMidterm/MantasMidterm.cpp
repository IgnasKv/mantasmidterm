#include "pch.h"
#include "Tree.h"
#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
using namespace std;

void ReadData(string fv, Tree S[], int &n);
void DisplayData(Tree S[], int n, int max, int aver, int many);
int Max(Tree S[], int n);
int Aver(Tree S[], int n);
int Many(Tree S[], int n);

const int max = 100;

int main()
{ 
	//Initial tree data
	Tree trees[max];
	int n = 0;
	ReadData("trees1.txt", trees, n);
	//Calculations
	int maxHeight = Max(trees, n);
	int aver = Aver(trees, n);
	int many = Many(trees, n);
	//Printing out data
	DisplayData(trees, n, maxHeight, aver, many);	
}

void ReadData(string fv, Tree S[], int &n) {
	ifstream fd(fv);
	fd >> n;
	for (int i = 0; i < n; i++)
	{
		fd >> S[i].name;
		fd >> S[i].height;
	}
	fd.close();
}

int Max(Tree S[], int n) {
	double maxHeight = INT_MIN;

	for (int i = 0; i < n; i++) {
		if (maxHeight < S[i].height) {
			maxHeight = S[i].height;
		}
	}

	return maxHeight;
}

int Aver(Tree S[], int n) {
	int sum = 0;
	int aver = 0;

	for (int i = 0; i < n; i++) {
		sum += S[i].height;
	}

	aver = sum / n;

	return aver;
}

int Many(Tree S[], int n) {
	int count = 0;

	for (int i = 0; i < n; i++) {
		if (S[i].height > 30) {
			count++;
		}
	}

	return count;
}

void DisplayData(Tree S[], int n, int max, int aver, int many) {
	const string separator = "|--------------|--------|\n";
	cout << separator;
	cout << "| Name         | Height |" << endl;
	for (int i = 0; i < n; i++) {
		cout << separator;
		cout << "| " << left << setw(13) << S[i].name;
		cout << "| " << left << setw(7) << S[i].height << "|" << endl;
	}
	cout << separator << endl;
	cout << "The highest tree is " << max << " meters tall." << endl;
	cout << "The average tree height is " << aver << " meters." << endl;
	cout << "There are " << many << " trees higher than 30 meters." << endl;
}
